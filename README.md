# Project 4: Brevet time calculator with Ajax

Reimplement the RUSA ACP controle time calculator with flask and ajax.

Credits to Michal Young for the initial version of this code.

## ACP controle times

That's "controle" with an 'e', because it's French, although "control" is also accepted. Controls are points where a rider must obtain proof of passage, and control[e] times are the minimum and maximum times by which the rider must arrive at the location.   

The algorithm for calculating controle times is described here (https://rusa.org/pages/acp-brevet-control-times-calculator). Additional background information is given here (https://rusa.org/pages/rulesForRiders). The description is ambiguous, but the examples help.  

We are essentially replacing the calculator here (https://rusa.org/octime_acp.html). We can also use that calculator to clarify requirements and develop test data.  

### Table for minimum and maximum speeds for ACP brevets

| Control Location(km) | Minimum Speed(km/hr) | Maximum Speed(km/hr) |
|:---:|:---:|:---:|
|0-200|15|34|
|200-400|15|32|
|400-600|15|30|
|600-1000|11.482|28|

## Clarifying Information

* Times and distances are rounded to the nearest whole number
* The min/max speeds are used in brackets:
	* A 300km brevet will use the same minimum speed for the whole distance but a max speed of 34 km/hr for the first 200 km and a max speed of 32 km/hrfor the last 100 km
	* The previous bracket will be used in it's entirety before moving to the next bracket
* By rule the closing time for the starting control is one hour after after the official start time
	* It is possible to have the first control after the start close before the start closes (admin should avoid this)
* The Closing times for the end of the brevet are not calculated as above and instead are as listed below (hr:min):
	* 13:30 for 200km
	* 20:00 for 300km
	* 27:00 for 400km
	* 40:00 for 600km
	* 75:00 for 1000km



Author: Meghan Riehl
Contact: mriehl5@uoregon.edu
